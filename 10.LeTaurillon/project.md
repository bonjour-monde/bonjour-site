---
title: Le Taurillon
taxonomy:
    tag: [Édition]
    category: [Édition]
date: 01-02-2017

---
Identité, mise en page et illustrations pour le Taurillon en Seine, un trimestriel publié par les Jeunes Européens de Paris. Le caractère de titrage a été crée par nos soins tandis que le texte se compose en Volkorn. Les illustrations, souvent proches de l'ornement, viennent rythmer et concrétiser les articles denses et techniques.

![](images/01.png)
![](images/02.png)
![](images/03.png)
![](images/04.png)
![](images/05.png)
![](images/06.png)
![](images/07.png)
