---
title: Les écritures post-coloniales
taxonomy:
    tag: [Web]
    category: [Web]
date: 02-02-2018

---
L’affiche numérique réalisée pour *Les écritures post-coloniales*, un évènement organisé par la Villa Gillet, reprend plus explicitement la gravure de Goya accompagnant l’événement et permets de découvrir le programme ainsi que des fragments poétiques.     

→ *[Affiche numérique](http://ressources.bonjourmonde.net/_ressources/_villagillet/18_ecriturePostColoniales) | [Villa Gillet](http://villagillet.net)*

![](images/01.png)
![](images/02.png)
![](images/03.png)
