---
title: Conférences du Lycée du Parc
taxonomy:
    tag: [Affiche]
    category: [Affiche]
date: 01-09-2016

---

Affiches des conférences de biologie et de physique du Lycée du Parc à Lyon. Les styles d'illustrations, renouvelées d'années en années, oscillent entre figuration et abstraction, informant les élèves tout en les amenant à décrypter les dessins.

![](images/01.png)
