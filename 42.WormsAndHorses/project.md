---
title: Worms & Horses
taxonomy:
    tag: [Atelier,Édition]
    category: [Atelier,Édition]
date: 17-06-2019

---
Invités au festival Liminal (Bucharest, Ro), nous avons proposé l'atelier *Worms & Horses* au cours duquel chaque participant était amené à confronter des comportements de virus informatiques à des aspects de sa propre pratique artistique. L'ensemble est réuni dans une publication imprimée dont le design, entièrement fait dans le navigateur à l’aide de l’outil [Page.JS](https://visionmedia.github.io/page.js/), suit les contraintes de l’atelier.

→ *[Liminal Festival](http://liminal.ro/2019/#theunseen)* / *[worms and horses](http://wormsandhorses.bonjourmonde.net/wormsandhorses/)*

![](images/BonjourSite-WormsandHorses-planche.png)
![](images/BonjourSite-WormsandHorses-planche2.jpg)
![](images/BonjourSite-WormsandHorses-planche3.jpg)
![](images/BonjourSite-WormsandHorses-planche4.png)
![](images/BonjourSite-WormsandHorses-planche5.jpg)
![](images/BonjourSite-WormsandHorses-planche6.png)
![](images/BonjourSite-WormsandHorses-planche7.png)
![](images/BonjourSite-WormsandHorses-planche8.png)
![](images/BonjourSite-WormsandHorses-planche9.png)
![](images/BonjourSite-WormsandHorses-planche10.png)
![](images/BonjourSite-WormsandHorses-planche11.png)
![](images/BonjourSite-WormsandHorses-planche12.png)
