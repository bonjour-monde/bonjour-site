---
title: Coder c'est écrire
taxonomy:
    tag: [Atelier]
    category: [Atelier]
date: 01-06-2017

---
Nous avons imaginé, pour les *Assises Internationales du Roman* 2017 organisées à Lyon par la Villa Gillet, un exercice d’écriture sans clavier : la création de textes uniquement à coups de copier/coller et à partir d’une source unique, Wikipédia. Après avoir composé un texte, les participants étaient invités à en modifier l’aspect à l’aide de petits morceaux de code CSS.

→ [Résultats](http://51.254.121.46/_ressources/coderEcrire/) | [Villa Gillet](http://villagillet.net)

![](images/air02.png)
![](images/air03.png)
![](images/thumb.jpg)
