---
title: Affiches numériques 2019
taxonomy:
    tag: []
    category: [web]
date: 31-01-2019

---
L’affiche numérique réalisée pour la Nuit de la Théologie 2019, associe au trois grand monothéismes un signe de la main particulier qui vient s’ajouter à l’image de l’évenement, elle-même basée sur une main tirée d’une oeuvre d’art religieuse.

![](images/01.png)
![](images/02.jpg)
![](images/03.jpg)
