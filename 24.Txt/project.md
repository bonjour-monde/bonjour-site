---
title: .Txt 3
taxonomy:
    tag: [Édition]
    category: [Édition]
date: 03-03-2018

---
Édité par B42, *.Txt* est une publication des Beaux-arts de Valence pensée comme un espace écriture pour les étudiants de l’option Design Graphique. Nous avons pris le parti de concevoir la mise en page de ce numéro comme un flux mouvant, dont les règles apparaissent en filigrane par des éléments dans et hors la grille. La mécanique, parfois, devient distraite. L’ouvrage, tout en restant lisible, vient nous rappeler qu’il n’est encore certains de vouloir se fixer dans une forme, comme dans un état de recherche perpétuel.
En collaboration avec Clarisse Podesta.     

→ *[B42](https://editions-b42.com/produit/txt-3/) | [ESAD•Grenoble•Valence](http://www.esad-gv.fr/fr/)*

![](images/01.png)
![](images/02.png)
![](images/03.png)
![](images/04.png)
