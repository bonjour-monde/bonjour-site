---
title: Détournement de fontes
taxonomy:
    tag: [Atelier]
    category: [Atelier]
date: 01-03-2017

---
Nous avons proposé, à l’occasion du *Mirage Festival* 2017, de porter un regard nouveau à la fois sur l’apparence d’un texte et sa matérialité en utilisant les valeurs d’un fichier vectoriel comme porte d’entrée vers de multiples expériences. Les participants ont été invités à manipuler le cœur même d’un encodage de caractère typographique afin de produire des formes surprenantes, questionnant les modèles habituels, les détruisant même parfois. La prise en main de DataFace, un petit outil semi-génératif diffusé depuis sous licence libre, a été également l’occasion d’une introduction aux joies du Terminal.

→ [Résultats](http://51.254.121.46/_ressources/detournementDeFontes/) | [DataFace](https://gitlab.com/bonjour-monde/dataface) | [Mirage Festival](http://www.miragefestival.com/)

![](images/04.JPG)
![](images/05.JPG)
![](images/06.png)
![](images/thumb.jpg)

Avec : Aurélie Cousquer, Celia Grandhomme, Gaspard Ollagnon, Jessica-Maria Nassif, Roger Gaillard, Tanja Reiterer
