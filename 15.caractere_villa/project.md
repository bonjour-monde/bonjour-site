---
title: Caractère typographique «Villa»
taxonomy:
    tag: [Typographie,Identité]
    category: [Typographie,Identité]
date: 01-01-2018
---
Pour fonder l’identité de la Villa Gillet, nous avons crée un caractère, le Villa. Sa version Regular reprend des formes archétypales de modèle typographique historique tandis que son Irregular se base sur des formes oubliées de l’Histoire.

![](images/LeCombo-Villa-typeface-01.png)
![](images/LeCombo-Villa-typeface-02.png)
![](images/LeCombo-Villa-typeface-03.png)
![](images/LeCombo-Villa-typeface-04.png)
![](images/LeCombo-Villa-typeface-05.png)
![](images/LeCombo-Villa-typeface-06.png)
![](images/LeCombo-Villa-typeface-07.png)
