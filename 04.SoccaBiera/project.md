---
title: Socca Bièra
taxonomy:
    tag: [Identité,Web]
    category: [Identité,Web]
date: 01-02-2016

---
Conception de l’identité visuelle de la marque de bière Niçoise Socca Biera, une bière aux pois chiches. Les illustrations sont dessinées à la souris et caractère typographique, le Career d’Antoine Gelgon, subit le même traitement.
→ [Visiter le site](http://www.soccabiera.fr/)

![](images/01.png)
![](images/02.png)
![](images/03.png)
![](images/04.png)
![](images/05.png)
![](images/06.png)
