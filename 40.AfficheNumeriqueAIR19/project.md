---
title: Affiche numérique AIR
taxonomy:
    tag: [Web]
    category: [Web]
date: 20-05-2019

---
L’affiche numérique des Assises Internationales du Roman 2019 reprend la forme du livre et de la trace en jeu sur l’identité de l’évenement et y ajoute un jeu autour des ponctuations.

*→ [Voir l’affiche numérique](http://ressources.bonjourmonde.net/_ressources/_villagillet/19_air/) | [Villa Gillet](http://villagillet.net)*

![](images/01.PNG)
![](images/02.PNG)
![](images/03.PNG)
