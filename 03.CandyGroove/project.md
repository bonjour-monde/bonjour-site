---
title: Badland
taxonomy:
    tag: [Affiche]
    category: [Affiche]
date: 01-01-2015

---
Identité visuelle du label de musique lyonnais Candy Groove, spécialisé dans le hip-hop et la bass music. Deux caractères typographiques exclusifs ont été dessinés. Par la suite, plusieurs séries d’affiches ont été conçues, pour chaque série d’évènements et de concerts.


![](images/01.png)
![](images/02.png)
![](images/03.png)
