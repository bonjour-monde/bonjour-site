---
title: Gutenbug
taxonomy:
    tag: [Atelier]
    category: [Atelier]
date: 01-12-2016

---
L’atelier Gutenbug, organisé dans les ateliers des Grands Voisins à Paris, proposait de regarder l’imprimante jet-d’encre non plus comme un simple module de sortie mais comme un outil production graphique. Une machine du quotidien, un périphérique connu de tous, discrètement posé dans un coin de notre bureau et qui incarne l’idée d’obsolescence programmée et de privatisation des codes sources ; ce même périphérique qui lança Richard Stallman sur la voie du libre.

→ [Les Grands Voisins](http://lesgrandsvoisins.org/) | [Richard Stallman & les imprimantes](https://www.oreilly.com/openbook/freedom/ch01.html)

![](images/thumb.jpg)
![](images/gutenbug02.jpg)
![](images/gutenbug03.jpg)
![](images/gutenbug04.jpg)
![](images/gutenbug05.jpg)
![](images/gutenbug06.jpg)

Avec : Gentien Arnault, Julie Blanc, Pauline Lecerf, Aurane Loury, Ivan Murit, Camille Peyrachon-Mouisset, Olivain Porry, Raphaël Seguin, Sabrine Sidki
