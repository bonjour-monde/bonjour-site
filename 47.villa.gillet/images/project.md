---
title: Programme Villa Gillet Jan—Mai
taxonomy:
    tag: []
    category: [Identité]
date: 01-01-2020

---
Le programme du début d’année 2020 de la Villa Gillet permet de revenir sur ce qui fait l’institution à l’occasion d’un changement de direction.

![](images/01.png)
![](images/02.png)
![](images/03.png)
![](images/04.png)
![](images/05.png)
