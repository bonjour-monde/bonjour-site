---
title: Synesthésie
taxonomy:
    tag: [Identité]
    category: [Identité]
date: 03-12-2016

---
Nous travaillons depuis 2016 avec le centre d’art Synesthésie, situé à Saint-Denis (93). Nous avons imaginé l’ensemble de leurs supports de communication, web et imprimés. L’identité du lieu s’articule autour du Syne — une famille typographique dessinée exclusivement et disponible à tous en open-source — et de la notion d’habitude, en faisant bégayer les différents éléments, de la lettre à la page.

→ [Synesthésie](http://synesthesie.com) | [Syne, caractère typographique open-source](https://gitlab.com/bonjour-monde/syne-typeface)

![](images/2017-10-30-Synesthesie-afficheNouveauLieu-04.jpg)
![](images/2017-11-29-Insta-ESIPEconference.jpg)
![](https://gitlab.com/bonjour-monde/syne-typeface/raw/master/img/syne-typeface-06.jpg)
![](images/thumb.jpg)
![](images/thumbnailmmaintenant.png)
