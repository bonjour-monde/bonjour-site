---
title: Extraits
taxonomy:
    tag: [Édition]
    category: [Édition]
date: 01-06-2016

---
Édition iconographique usant des images hautes définitions d’œuvres dans le domaine publique en ligne. Chaque Extrait est le produit d’une grille appliqué à un tableau, faisant rejaillir des détails inattendus ou des composition abstraites de tableaux de la Renaissance. Un court texte ancien vient éclairer le thème de chaque œuvre.

![](images/0.gif)
![](images/01.png)
![](images/1.gif)
