---
title: Revue Pli
taxonomy:
    tag: [Web]
    category: [Web]
date: 09-09-2017

---
Création du site internet de Pli, revue consacrée aux interactions entre architecture et édition. La navigation, basée sur le temps, est rythmée par les couleurs successives des différents numéros de la revue. Une timeline colorée permet ainsi de naviguer entre les saisons délimitées par les publications annuelles.     

*-> [Pli — Revue](http://http://plirevue.com//)*


![](images/01.png)
![](images/02.png)
![](images/03.png)
![](images/04.png)
![](images/05.png)
![](images/06.png)
![](images/07.png)
![](images/08.png)
