---
title: Programme Villa Gillet Sept—Déc
taxonomy:
    tag: [Édition]
    category: [Édition]
date: 01-01-2018

---
En 2018, nous refondons l’identité de la Villa Gillet avec un caractère, le Villa, mais aussi un ponctuation inventée pour chaque saison. Ce principe est introduit par un marque-page avant d’être explicité dans le programme.

![](images/00.png)
![](images/01.png)
![](images/02.png)
![](images/03.png)
![](images/04.png)
![](images/05.png)
