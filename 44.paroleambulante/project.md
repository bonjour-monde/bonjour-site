---
title: Parole Ambulante
taxonomy:
    tag: [Identité,Affiche]
    category: [Identité,Affiche]
date: 20-09-2019

---
Le festival de poésie Parole Ambulante nous a confié l’identité visuelle de son édition 2019. Autour du thème “Le lierre et l’asphalte” nous avons imaginé un procédé de création d’images à la croisée de l’algorithmique et du végétal. 

→ *[Espace Pandora](https://espacepandora.org/lassociation/)* | *[Vinny](https://gitlab.com/bonjour-monde/tools/vinny)*

![](images/01.png)
![](images/02.jpg)
![](images/03.png)
![](images/04.jpg)
![](images/05.jpg)
![](images/06.jpg)
![](images/08.jpg)
