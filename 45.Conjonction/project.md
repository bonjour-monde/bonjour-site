---
title: Conjonction
taxonomy:
    tag: []
    category: [Web]
date: 01-10-2019

---
Afin d’accueillir des articles, traduits ou écrits, sur la magie et le design, nous avons réalisé le site Conjonction.org qui permet d’accéder à ces textes, illustrés et annotés, mais aussi de générer des pdfs pour une lecture papier. On y trouve des écrits de Lovecraft, Alan Moore, Alister Crowley, etc.

→ *[Conjonction](http://conjonction.org/)*
![](images/01.PNG)
![](images/02.PNG)
![](images/03.PNG)
![](images/04.PNG)
