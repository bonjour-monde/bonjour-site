---
title: Passeport Sortie
taxonomy:
    tag: []
    category: [Edition]
date: 01-01-2020

---
Le Passeport Sortie, conçu pour accompagner des anciens détenus à se ré-insérer dans la société, a été réalisé pour le CSAPAA antenne Toxicomanie de l’Hôpital du Vinatier.

![](images/01.png)
![](images/02.png)
![](images/03.png)
![](images/04.png)
![](images/05.png)
![](images/06.png)
