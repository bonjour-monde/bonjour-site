---
title: Assises Internationales du Roman
taxonomy:
    tag: [Identité,Affiche,Édition]
    category: [Identité,Affiche,Édition]
date: 20-05-2019

---
Réalisation de l’identité des Assises Internationales du Roman 2018. Les affiches reprenaient le principe du signe, imaginé cette année là.   

→ *[Villa Gillet](http://villagillet.net)*

![](images/01.jpg)
![](images/02.png)
![](images/03.png)
![](images/04.jpg)
