---
title: Journées du Logiciel Libre
taxonomy:
    tag: [Identité,Web]
    category: [Identité,Web]
date: 03-03-2018

---
Communication colorée pour les vingt ans des Journées du Logiciel Libre, réalisée uniquement à l'aide d'outils libres, logiciels et caractères typographiques. Déployé sur différents supports web et imprimé, le système graphique s'est également mué en signalétique sur l'ensemble de l'évènement.     
→ *[Journées du Logiciel Libre](https://jdll.org/)*

![](images/01.png)
![](images/08.gif)
![](images/05.png)
![](images/06.png)
![](images/07.png)
