---
title: Atelier Champs Libres
taxonomy:
    tag: [Identité,Typographie]
    category: [Identité,Typographie]
date: 01-09-2017

---
Identité visuelle, caractère typographique et site-vitrine pour l’artisan menuisier Jacques-Louis Belliard, inspirés par ses créations aux formes contrastées et aux lignes dynamiques.      

→ *[Visiter le site](http://atelierchampslibres.com)*

![](images/LeCombo-AtelierChampsLibres-01.png)
![](images/LeCombo-AtelierChampsLibres-02.png)
![](images/LeCombo-AtelierChampsLibres-03.png)
![](images/LeCombo-AtelierChampsLibres-04.png)
