---
title: La Chose Publique
taxonomy:
    tag: [Identité,Édition]
    category: [Identité,Édition]
date: 11-11-2018

---
La Chose Publique est un évenement co-organisé par la Villa Gillet. Nous avons imaginé de mettre en image la démocratie, thème fort du festival, sous la forme d'une foule en dessinant l'ensemble des intervenants, ainsi que l'équipe organisatrice pour les intégrer dans une masse de visages anonymes mais caractérisés.    

→ *[Villa Gillet](http://villagillet.net)*

![](images/00.jpg)
![](images/01.png)
![](images/1.png)
![](images/2.png)
![](images/04.jpg)
![](images/05.jpg)
