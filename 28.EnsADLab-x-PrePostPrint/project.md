---
title: EnsADLab x PrePostPrint
taxonomy:
    tag: [Atelier]
    category: [Atelier]
date: 01-04-2018

---

Le laboratoire de recherche de l'École nationale supérieure des Arts Décoratifs de Paris invitait PrePostPrint, une initiative liée aux procédés de création graphique et systèmes de publication libres considérés comme *alternatifs*, pour deux jours de travail en commun et de conférences. Nous y avons présenté ‘Donner à voir’, un regard sur la médiation et le rapport au groupe dans nos processus d'exploration.


→ [Captation vidéo](https://vimeo.com/292900706) | [EnsADLab](http://www.ensadlab.fr/) | [PrePostPrint](https://prepostprint.org/doku.php//en/introduction)

![photo EnsADLab x PrePostPrint](images/ensadlabprepostprint00.jpg)
![photo EnsADLab x PrePostPrint](images/ensadlabprepostprint01.jpg)
![photo EnsADLab x PrePostPrint](images/ensadlabprepostprint02.jpg)
