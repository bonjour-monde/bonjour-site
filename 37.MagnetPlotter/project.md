---
title: Atelier plotter
taxonomy:
    tag: [Atelier]
    category: [Atelier]
date: 09-02-2019

---
À l’occasion des *[Expériences numériques](https://epn.salledesrancy.com/experiences-numeriques/)* 2019, nous avons animé un stand de dessin sur ordinateur. À l’aide du logiciel *[Magnet Mike](https://gitlab.com/armansansd/magnetmike)* les visiteurs pouvaient transformer leurs dessins sur une grille pour ensuite l'imprimer à la table traçante.


![](images/01.jpg)
![](images/02.jpg)
![](images/03.jpg)
