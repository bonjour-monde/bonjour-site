---
title: Assises Internationales du Roman
taxonomy:
    tag: [Identité,Affiche,Édition]
    category: [Identité,Affiche,Édition]
date: 04-04-2018

---
Réalisation de l’identité des Assises Internationales du Roman 2018. Déployées dans la ville, les affiches reprenaient le signe développé pour la Villa Gillet cette année-là.     

→ *[Villa Gillet](http://villagillet.net)*

![](images/01.jpg)
![](images/02.png)
![](images/03.png)
![](images/04.png)
![](images/05.png)
![](images/06.jpg)
