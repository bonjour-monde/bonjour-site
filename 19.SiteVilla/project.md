---
title: Site de la Villa Gillet
taxonomy:
    tag: [Web]
    category: [Web]
date: 01-02-2018

---
Refonte du site de la Villa Gillet basée sur le nouveau caractère dessiné par nos soins. Le site devait pouvoir s’adapter aux différents contenus, passés et à venir. La navigation repose essentiellement sur cette donnée temporelle.    
→ *[visiter le site](http://villagillet.net/)*

![](images/01.png)
![](images/02.png)
![](images/03.png)
![](images/04.png)
![](images/05.png)
![](images/06.png)
![](images/07.png)
![](images/08.png)
