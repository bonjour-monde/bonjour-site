---
title: Affiche numérique AIR
taxonomy:
    tag: [Web]
    category: [Web]
date: 04-04-2018

---
L’affiche numérique des Assises Internationales du Roman reprend la visuel de l’affiche de manière interactive et permet de découvrir l’ensemble du programme.    
→ *[Voir l'affiche numérique](http://ressources.bonjourmonde.net/_ressources/_villagillet/18_AIR/)*

![](images/00.gif)
![](images/01.jpg)
![](images/02.jpg)
