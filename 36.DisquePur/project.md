---
title: Disque Pur
taxonomy:
    tag: [Édition]
    category: [Édition]
date: 27-04-2019

---

Science-fiction informatique, *Disque Pur* est un livre écrit par ![Lucas Lejeune](https://lucaslejeune.com/Disque-pur) durant une résidence dans l'ancienne synagogue de Forbach. Un démon informatique traverse un disque dur organique, et croise ici et là, des surprises poétiques étonnantes.

![Photo edition Disque pur](images/1.png)
![Photo edition Disque pur](images/2.png)
![Photo edition Disque pur](images/3.png)
![Photo edition Disque pur](images/4.png)
![Photo edition Disque pur](images/5.png)
![Photo edition Disque pur](images/6.png)
![Photo edition Disque pur](images/7.png)
![Photo edition Disque pur](images/8.png)
![Photo edition Disque pur](images/9.png)
![Photo edition Disque pur](images/10.png)
