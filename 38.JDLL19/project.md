---
title: Journées du Logiciel Libre
taxonomy:
    tag: [Identité,Web]
    category: [Identité,Web]
date: 06-04-2019

---
Communication colorée pour les *Journées du Logiciel Libre*, réalisée uniquement à l'aide d'outils libres, logiciels et caractères typographiques. Cette année, le thème était *Ecologeek: pour une terre communautaire* et nous avons réalisé les impressions des affiches en Risographie chez *Toner Toner*. Déployé sur différents supports web et imprimé, le système graphique s'est également mué en signalétique sur  réalisé par [Chateau Fort Fort](http://www.chateaufortfort.fr/)

→ *[Journées du Logiciel Libre](https://jdll.org/)*

![](images/01.png)
![](images/02.png)
![](images/03.png)
![](images/03.jpg)
![](images/04.png)
![](images/05.jpg)
![](images/06.jpg)
![](images/07.jpg)
