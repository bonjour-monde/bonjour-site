---
title: Code X
taxonomy:
    tag: [Édition,Typographie]
    category: [Édition,Typographie]
date: 14-08-2019

---
Nous avons réalisé la maquette du deuxième numéro de Code X, revue sur l'archéologie des média, publiée par les éditions HYX. La mise en page a été créée entièrement avec paged.js, en HTML, CSS et javascript. Ce travail a été l'occasion de jouer de la surutilisation et la superposition de règles CSS, et nous a amené à concevoir une famille typographique pour l'occasion : Le FerroForje. La famille se caractérise par des dessins de plus en plus contraints, selon une grille pré-établie. 

→ *[Code X n°2](http://www.editions-hyx.com/fr/code-x-0)*

![](images/01.png)
![](images/02.png)
![](images/03.png)
![](images/04.png)
![](images/04_2.png)
![](images/05.png)
