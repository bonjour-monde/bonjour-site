---
title: Dix lettres sauvées
taxonomy:
    tag: [Édition,Typographie]
    category: [Édition,Typographie]
date: 26-05-2019

---
À la demande de la Villa Gillet et de la Fondation Saint-Irénée, nous avons mis en page la publication *Dix lettres sauvées*, qui rassemble les textes de dix auteurs internationaux. Nous avons choisi d'accompagner chaque proposition d'un lettrage.

→ *[Villa Gillet](http://villagillet.net) | [Fondation Saint-Irénée](https://fondationsaintirenee.org/)*

![](images/BonjourSite-LettresSauvees-planches.png)
![](images/BonjourSite-LettresSauvees-planches2.png)
![](images/BonjourSite-LettresSauvees-planches3.png)
![](images/BonjourSite-LettresSauvees-planches4.png)
