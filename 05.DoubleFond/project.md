---
title: Double fond des signes
taxonomy:
    tag: [Édition]
    category: [Édition]
date: 01-06-2016

---
Mise en page d'un mémoire de fin d’étude sur la Monade Hiéroglyphique, un signe ésotérique crée par John Dee en 1564. Le livre est recouvert par un poster comprenant l'intégralité du texte de John Dee sur la création de la Monade. L’iconographie est intégré directement dans le texte. Ce dernier est composé en Sabot de Martin Violette et le Mistral de Roger Excoffon y fait office d’italique.

![](images/alchimie1.png)
![](images/alchimie2.png)
![](images/alchimie3.png)
![](images/alchimie4.png)
