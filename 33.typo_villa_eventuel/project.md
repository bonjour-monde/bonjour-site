---
title: L'Éventuel
taxonomy:
    tag: [Typographie,Web]
    category: [Typographie,Web]
date: 01-01-2019

---
Nous avons créée, avec la Villa Gillet, une plateforme de dessin participative où chacun pouvait imaginer les évolutions futurs de l’alphabet latin. Les contributions ont nourri le dessin de la déclinaison grasse du caractère typographique Villa. Les 2800 dessins ont été présentés sur une bâche lors des Assises Internationales du Roman 2019.


→ *[L'Éventuel](http://eventuel.villagillet.net/) | [Villa Gillet](http://villagillet.net)*

![](images/01.png)
![](images/02.png)
![](images/03.png)
![](images/04.jpg)
