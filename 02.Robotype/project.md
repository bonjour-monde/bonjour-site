---
title: Robotype
taxonomy:
    tag: [Atelier]
    category: [Atelier]
date: 01-01-2014

---
Des tracés vectoriels, projetés sur un plan horizontal ou directement transcodés en commande Arduino, sont proposés à l’interprétation d’un robot suiveur de ligne armé d'un outil de tracé.    
Ne réalisant au départ que des formes simples destinées à paramétrer la machine, comprendre ses possibilités et ses limites, le RoboType est devenu capable de dessiner des lettres, signes complexes mais pour lesquels les choix de design sont limités par la contrainte d’appartenance à un système connu et normé.    
Le jeu devient alors de produire une lettre portant les caractéristiques de l'outil qui l’a tracée, tout en restant reconnaissable.Le résultat n'est plus uniquement guidé par le ductus et l’outil, comme en calligraphie traditionnelle, mais également par de multiples facteurs plus ou moins prévisibles : sens du parcours, position des capteurs, hésitations.
Le Robotype a été activé et présenté à plusieurs reprises, notamment lors des Expériences Numériques à Lyon.

~> [Documentation](https://gitlab.com/bonjour-monde/robotype) | [Expériences Numériques](https://epn.salledesrancy.com/experiences-numeriques/)

![](images/robotype.jpg)
![](images/robotype01.jpg)
![](images/thumb.jpg)
![](images/robotype03.gif)
