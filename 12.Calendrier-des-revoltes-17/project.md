---
title: Calendrier des Révoltes
taxonomy:
    tag: [Édition]
    category: [Édition]
date: 01-04-2017

---
Prenant la suite de Vier5 et Jérôme Saint-Loubert Bié, nous avons collaboré avec l’artiste Matthieu Saladin sur la mise en forme de son *Calendrier des révoltes* 2017, la troisième activation de ce projet “où les informations habituelles relatives à chaque jour laissent place à l’unique mention d’une révolte ayant eu lieu ce même jour.” Les perturbations génératives de la mise en page sont liées à un enregistrement sonore pris devant un bureau de vote de Saint-Denis lors du premier tour des éléctions présidentielles de 2017. De nombreuses versions ont ainsi été produites, en français, anglais et arabe. Le *Calendrier des Révoltes* 2017 a été produit par le centre d'art Synesthésie ¬ MMAINTENANT

→ [Matthieu Saladin](http://www.matthieusaladin.org/) [Synesthésie](http://www.synesthesie.com/) [Code source du programme de distorsion](https://gitlab.com/bonjour-monde/revolte)_

![](images/03.JPG)
![](images/02.JPG)
![](images/thumb.jpg)
