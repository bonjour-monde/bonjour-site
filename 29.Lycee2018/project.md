---
title: Conférences du Lycée du Parc
taxonomy:
    tag: [Affiche]
    category: [Affiche]
date: 09-09-2018

---

Affiches des conférences de biologie et de physique du Lycée du Parc à Lyon. Les styles d'illustrations, renouvelés d'année en année, oscillent entre figuration et abstraction, informant les élèves tout en les amenant à décrypter les dessins.    

→ *[Lycée du Parc](http://lyceeduparc.fr/)*

![](images/01.png)
