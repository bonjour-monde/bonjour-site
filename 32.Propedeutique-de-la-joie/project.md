---
title: Propédeutique de la joie
taxonomy:
    tag: [Atelier]
    category: [Atelier]
date: 20-01-2019

---

Nous avons été conviés à l’Esad Orléans, à l’occasion des *Grands ateliers de janvier*, pour travailler l’écriture et ses formes numériques en compagnie de l’écrivain Antoine Dufeu.
Le thème de la joie nous a servi de fil conducteur et nous avons ainsi accompagné une vingtaine d’étudiants dans la conception et la médiatisation de textes, le plus souvent sur écran, pour penser conjointement le fond et la forme de leur écrit.


→ [Résultats](http://51.254.121.46/_ressources/esad-orleans/) | [Grands ateliers de janvier](https://www.esad-orleans.fr/grands-ateliers-de-janvier/) | [Antoine Dufeu](http://www.antoinedufeu.fr/)

![photo Esad Orléans](images/esador.jpg)

Avec : Adrien Bisecco, Morgane Bardel, Nicolas Lemaitre, Basile Jesset, Salomé Cardoso, Daphné Paris, Anais Quintanilla Del Mar, Brice Kaptur, Camille Savre, Julia Campo, Nicolas Besse, Karolina Borkowska, Jacques Bourget, Eva Vedel, Lorenzo Tondini, Lorraine Auclert, Benjamin Riot, Valentine Report, Camille Vidal.
