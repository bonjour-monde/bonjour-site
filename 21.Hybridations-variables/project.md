---
title: Hybridations variables
taxonomy:
    tag: [Atelier]
    category: [Atelier]
date: 03-03-2018

---

Invités à l’EnsAD Paris dans le cadre du module de recherche *Libres hybridations* nous avons réactivé les procédés expérimentaux de manipulation typographique, amélioré notre programme [DataFace](https://gitlab.com/bonjour-monde/dataface) et intégré les nouvelles propriétés de fontes variables permises par le format OpenType.
Le module de recherche a été initié par Julie Blanc, Lucile Haute et Quentin Juhel.


→ [Hybridations in-use](http://51.254.121.46/_ressources/mrc-variable) | [DataFace](https://gitlab.com/bonjour-monde/dataface)

![](images/01.gif)
![](images/thumb.jpg)
![](images/03.JPG)

Avec : Louise Bailay, Émilie Lor, Claire Maroufin, Imrane Mertad, Louise Terrasse, Astou Toure, Chloé Vanderstraeten
