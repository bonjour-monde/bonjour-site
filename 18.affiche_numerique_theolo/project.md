---
title: La Nuit de la Théologie
taxonomy:
    tag: [Web]
    category: [Web]
date: 01-01-2018

---
L’affiche numérique réalisée pour la Nuit de la Théologie reprend l’image de main associée à l’évènement ainsi que trois signe de main correspondant au trois grands monothéismes. Les mains permettent de découvrir le programme de la nuit.     

→ *[Affiche numérique](http://ressources.bonjourmonde.net/_ressources/_villagillet/18_nuitDeLaTheologie) | [Villa Gillet](http://villagillet.net)*

![](images/01.png)
![](images/02.png)
