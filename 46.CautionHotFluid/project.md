---
title: Caution Hot Fluid
taxonomy:
    tag: []
    category: [Atelier]
date: 01-12-2019

---
Invité à l’Esad Pau, nous avons proposé *Caution Hot fluid*, un Workshop ressuscitant le volumen à l’aide de machine à impression thermique et de rouleau de tickets de caisse, le tout à partir d’un fil de forum culte du web des années 2000, le récit de John Titor (aka *time_traveler_01*). Ce fut surtout l’occasion d’aborder les publications hybride web-imprimé à l’aide la mise en page web-CSS.

![](images/01.png)
![](images/02.png)
![](images/03.png)
![](images/04.png)
